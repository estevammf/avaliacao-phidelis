﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace AplicacaoTeste.Dados.Conexao
{
    public class DatabaseConnection : IDisposable
    {
        public IDbConnection Connection;
        public DatabaseConnection()
        {
            Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }

        public void Dispose()
        {
            Connection.Close();
        }
    }
}
