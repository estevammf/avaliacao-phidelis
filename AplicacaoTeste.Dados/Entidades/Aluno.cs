﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AplicacaoTeste.Dados.Entidades
{
    public class Aluno
    {
        public long IdAluno { get; set; }
        [DisplayName("Nome Completo: *")]
        [Required(ErrorMessage = "Campo de preenchimento obrigatório!")]
        public string NomeCompleto { get; set; }
        [DisplayName("Data de nascimento: *")]
        [Required(ErrorMessage = "Campo de preenchimento obrigatório!")]
        public DateTime DataDeNascimento { get; set; }
        [DisplayName("CPF: *")]
        [Required(ErrorMessage = "Campo de preenchimento obrigatório!")]
        [MaxLength(11, ErrorMessage = "Tamanho não pode ser maior que 11 caracteres")]
        public string Cpf { get; set; }
        [DisplayName("Matrícula: *")]
        [Required(ErrorMessage = "Campo de preenchimento obrigatório!")]
        [MaxLength(6, ErrorMessage = "Tamanho não pode ser maior que 6 caracteres")]
        public string Matricula { get; set; }

    }
}
