﻿using System;

namespace AplicacaoTeste.Dados.Entidades
{
    public class CursosAluno
    {
        public int IdCursosAluno { get; set; }
        public long IdAluno { get; set; }
        public int IdCurso { get; set; }
        public DateTime DataIngresso { get; set; }
    }
}
