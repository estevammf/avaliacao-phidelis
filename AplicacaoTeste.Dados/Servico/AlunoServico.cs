﻿using AplicacaoTeste.Dados.Conexao;
using AplicacaoTeste.Dados.Entidades;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace AplicacaoTeste.Dados.Servico
{
    public class AlunoServico
    {
        public List<Aluno> ObtemAlunos()
        {
            try
            {
                var query = "select * from Aluno order by IdAluno";
                using (var db = new DatabaseConnection())
                {
                    var lista = (List<Aluno>)db.Connection.Query<Aluno>(query);
                    return lista;
                }
            }
            catch
            {
                throw;
            }
           
        }

        //aplicar string interpolation
        public Aluno ObtemAluno(long id)
        {
            string query = string.Format("select * from Aluno where IdAluno = {0}", id);
            using (var db = new DatabaseConnection())
            {
                var aluno = db.Connection.Query<Aluno>(query).FirstOrDefault();
                return aluno;
            }
        }

        public void Delete(long id)
        {
            string query = string.Format($"delete from Aluno where IdAluno = {id}");
            using (var db = new DatabaseConnection())
            {
                db.Connection.Query(query);
            }
        }

        //aplicar string interpolation
        public void Update(Aluno aluno)
        {
            var query = string.Format("UPDATE ALUNO " +
                                      "SET NOME = '{0}'" +
                                      "WHERE IdAluno = {1}", aluno.NomeCompleto, aluno.IdAluno);

            using (var db = new DatabaseConnection())
            {
                db.Connection.Query(query);
            }
        }


        //aplicar string interpolation
        public int Insert(Aluno aluno)
        {
            var query = string.Format("SET DATEFORMAT DMY;INSERT INTO ALUNO (NOMECOMPLETO, DATADENASCIMENTO, CPF, MATRICULA) " +
                                      "VALUES ('{0}', CAST('{1}' as DATE), '{2}', '{3}'); " +
                                      "SELECT CAST(SCOPE_IDENTITY() as int);", aluno.NomeCompleto, aluno.DataDeNascimento,
                                      aluno.Cpf, aluno.Matricula);

            using (var db = new DatabaseConnection())
            {
                var codigo = db.Connection.Query<int>(query).Single();
                return codigo;
            }
        }
    }
}
