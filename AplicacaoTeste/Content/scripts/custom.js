/* Theme Name: The Project - Responsive Website Template
 * Author:HtmlCoder
 * Author URI:http://www.htmlcoder.me
 * Author e-mail:htmlcoder.me@gmail.com
 * Version:1.1.0
 * Created:March 2015
 * License URI:http://support.wrapbootstrap.com/
 * File Description: Place here your custom scripts
 */


$("a[data-modal]").on("click", function (e) {
    // hide dropdown if any (this is used wehen invoking modal from link in bootstrap dropdown )
    //$(e.target).closest('.btn-group').children('.dropdown-toggle').dropdown('toggle');

    $('#myModalContent').load(this.href, function () {
        $('#myModal').modal({
            backdrop: 'static'
            //keyboard: true
        }, 'show');
        //bindForm(this);
    });
    return false;
});

function modalLarge(url) {
    $('#myModalContent').load(url, function () {
        $('#myModal').modal({
            backdrop: 'static'
            //keyboard: true
        }, 'show');
    });
    return false;
};