﻿using AplicacaoTeste.Dados.Entidades;
using AplicacaoTeste.Dados.Servico;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AplicacaoTeste.Controllers
{
    public class AlunoController : Controller
    {
        private readonly AlunoServico _servico;
        public AlunoController()
        {
            _servico = new AlunoServico();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            try
            {
                var todosAlunos = _servico.ObtemAlunos();
                return PartialView("_List", todosAlunos);
            }
            catch (Exception e)
            {
                TempData["Error"] = e.Message;
                return PartialView("_List", new List<Aluno>());
            }
         
        }

        public ActionResult Cadastrar()
        {
           return View();
        }

        [HttpPost]
        public ActionResult Cadastrar(Aluno model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _servico.Insert(model);
                    if (result > 0)
                    {
                        TempData["Success"] = "Solicitação processada com sucesso!";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["Error"] = "Ocorreu um erro ao tentar processar a sua solicitação!";
                    }
                   
                }

                return View("Cadastrar", model);

            }
            catch (Exception e)
            {
                TempData["Error"] = "Ocorreu um erro ao tentar processar a sua solicitação!";
                return RedirectToAction("Index");
            }
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}